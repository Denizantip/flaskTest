from flask import Flask, redirect, url_for
from flask import render_template, request
import hashlib

app = Flask(__name__)

def get_hash256(filename):
    with open(filename, 'rb') as f:
        sha = hashlib.sha256()
        for line in f:
            sha.update(line)
    return sha.hexdigest()

@app.route('/', methods=['POST', "GET"])
def index():
    if request.method == "GET":
        return render_template('index.html')

@app.route('/hash', methods=["POST"])
def hash():
    if request.method == "POST" and request.files['f'].filename != '':
        filename = request.files['f'].filename
        path = "/home/denizantip/%s" % filename
        file = request.files['f']
        file.save(path)
        return render_template('index.html', hash = get_hash256(path))
    else:
        return

if __name__ == '__main__':
    app.run()
